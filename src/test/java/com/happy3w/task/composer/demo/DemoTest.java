package com.happy3w.task.composer.demo;

import com.happy3w.java.ext.MapBuilder;
import com.happy3w.task.composer.AnnotationTaskDetector;
import com.happy3w.task.composer.Task;
import com.happy3w.task.composer.TaskComposer;
import com.happy3w.task.composer.demo.tasks.ExportXxxFile;
import com.happy3w.task.composer.demo.tasks.LoadDatabaseConfig;
import com.happy3w.task.composer.demo.tasks.ParseAllSqls;

import java.util.Arrays;
import java.util.List;

public class DemoTest {
    public void test_main() {
        List<Task> tasks = AnnotationTaskDetector.detectTasks(LoadDatabaseConfig.class, new ParseAllSqls());
        TaskComposer taskComposer = TaskComposer.build(tasks);
        taskComposer.withValues(
                MapBuilder.of(InputParams.DatabaseConfigFile, (Object) "/a/b/config.json")
                        .and(InputParams.SqlFilePaths, Arrays.asList("/a/b/sqls", "/a/b/sql2"))
                        .build()
        ).evaluate(ExportXxxFile.Output);
    }
}

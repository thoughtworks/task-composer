package com.happy3w.task.composer.demo.tasks;

import com.happy3w.task.composer.TaskData;
import com.happy3w.task.composer.demo.InputParams;
import com.happy3w.task.composer.demo.model.SqlModel;

import java.util.List;
import java.util.stream.Collectors;

public class ParseAllSqls {
    public static final String SqlModels = "sql-models";
    public @TaskData(SqlModels) List<SqlModel> parseSqls(@TaskData(InputParams.SqlFilePaths) List<String> paths) {
        return paths.stream()
                .map(p -> new SqlModel(p))
                .collect(Collectors.toList());
    }
}

package com.happy3w.task.composer.demo.tasks;

import com.happy3w.task.composer.TaskData;
import com.happy3w.task.composer.demo.model.DatabaseInfo;
import com.happy3w.task.composer.demo.model.SqlModel;

import java.util.List;

public class ExportXxxFile {
    public static final String Output = "ExportXxxFile";

    public @TaskData(Output) String export(
            @TaskData(LoadDatabaseConfig.Output) DatabaseInfo info,
            @TaskData(ParseAllSqls.SqlModels) List<SqlModel> sqlModels) {
        return "ok";
    }
}

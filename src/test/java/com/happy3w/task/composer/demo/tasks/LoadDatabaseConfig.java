package com.happy3w.task.composer.demo.tasks;

import com.happy3w.task.composer.TaskData;
import com.happy3w.task.composer.demo.InputParams;
import com.happy3w.task.composer.demo.model.DatabaseInfo;

public class LoadDatabaseConfig {
    public static final String Output = "database-info";
    public static @TaskData(Output) DatabaseInfo load(@TaskData(InputParams.DatabaseConfigFile) String path) {
        return new DatabaseInfo(path);
    }
}

package com.happy3w.task.composer.demo;

public class InputParams {
    public static final String DatabaseConfigFile = "database-config-file";
    public static final String SqlFilePaths = "sql-file-paths";
}

package com.happy3w.task.composer.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SqlModel {
    private String path;
}

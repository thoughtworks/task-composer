package com.happy3w.task.composer;

public class AnnTypeConvert {
    public static final String N1 = "n1";
    public static final String N2 = "n2";
    public static final String N3 = "n3";

    public @TaskData(AnnTypeConvert.N1) Integer convert(@TaskData(InputParams.N1Text) String text) {
        return Integer.valueOf(text);
    }

    public static @TaskData(AnnTypeConvert.N2) Integer iconvert(@TaskData(InputParams.N2Text) String text) {
        return Integer.valueOf(text);
    }

    public static @TaskData(AnnTypeConvert.N3) Integer iconvert2(@TaskData(InputParams.N3Text) String text) {
        return Integer.valueOf(text);
    }
}

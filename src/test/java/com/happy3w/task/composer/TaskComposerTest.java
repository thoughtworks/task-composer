package com.happy3w.task.composer;

import com.happy3w.toolkits.reflect.ReflectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

class TaskComposerTest {
    TaskDataDef n1Text = new TaskDataDef("n1Text", String.class);
    TaskDataDef n1 = new TaskDataDef("n1", Integer.class);
    TaskDataDef n2Text = new TaskDataDef("n2Text", String.class);
    TaskDataDef n2 = new TaskDataDef("n2", Integer.class);
    TaskDataDef n3Text = new TaskDataDef("n3Text", String.class);
    TaskDataDef n3 = new TaskDataDef("n3", Integer.class);
    TaskDataDef s12 = new TaskDataDef("s12", Integer.class);
    TaskDataDef m23 = new TaskDataDef("m23", Integer.class);

    @Test
    void should_manual_build_tasks_and_run_success() {
        TaskComposer taskComposer = createTaskComposer();

        Integer s12Value = taskComposer
                .withValue(n1Text.getName(), "111")
                .withValue(n2Text.getName(), "222")
                .executorFor(s12.getName())
                .withThreadCount(2)
                .withWaitTime(200)
                .getDataValue(s12.getName());
        Assertions.assertEquals(333, s12Value.intValue());

        Integer m23Value = taskComposer
                .withValue(n3Text.getName(), "333")
                .executorFor(m23.getName())
                .getDataValue(m23.getName());
        Assertions.assertEquals(222 * 333, m23Value.intValue());
    }

    private TaskComposer createTaskComposer() {
        TaskComposer taskComposer = TaskComposer.build(Arrays.asList(
                MethodTask.from(
                        ReflectUtil.findMethod("convert", InstanceConvert.class.getDeclaredMethods()),
                        new InstanceConvert(),
                        Arrays.asList(n1Text),
                        Arrays.asList(n1)),
                MethodTask.from(
                        ReflectUtil.findMethod("convert", StaticConvert.class.getDeclaredMethods()),
                        null,
                        Arrays.asList(n2Text),
                        Arrays.asList(n2)),
                FunctionTask.from(
                        ntext -> Integer.valueOf(String.valueOf(ntext)),
                        Arrays.asList(n3Text),
                        TaskDataMode.SingleMode,
                        Arrays.asList(n3),
                        TaskDataMode.SingleMode
                        ),
                MethodTask.from(
                        ReflectUtil.findMethod("convert", StaticSum.class.getDeclaredMethods()),
                        null,
                        Arrays.asList(n1, n2),
                        Arrays.asList(s12)),
                MethodTask.from(
                        ReflectUtil.findMethod("convert", StaticMulti.class.getDeclaredMethods()),
                        null,
                        Arrays.asList(n2, n3),
                        Arrays.asList(m23)
                )
        ));
        return taskComposer;
    }

    @Test
    void should_gen_depend_success() {
        TaskComposer taskComposer = createTaskComposer();
        String itemText = taskComposer.dependItemList()
                .stream()
                .map(DependItem::formatGraphItem)
                .collect(Collectors.joining("\n"));

        Assertions.assertEquals("s12,n1,1\n" +
                "s12,n2,1\n" +
                "m23,n2,1\n" +
                "m23,n3,1", itemText);
    }

    public static class InstanceConvert {
        public Integer convert(String text) {
            return Integer.valueOf(text);
        }
    }

    public static class StaticConvert {
        public static Integer convert(String text) {
            return Integer.valueOf(text);
        }
    }

    public static class StaticSum {
        public static Integer convert(Integer n1, Integer n2) {
            return n1 + n2;
        }
    }

    public static class StaticMulti {
        public static Integer convert(Map<String, Object> params) {
            int v = 1;
            for (Object objV : params.values()) {
                v *= ((Integer) objV).intValue();
            }
            return v;
        }
    }

}

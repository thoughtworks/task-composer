package com.happy3w.task.composer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DependItemTest {

    @Test
    void should_sort_success() {
        List<DependItem> sorted = DependItem.sort(Arrays.asList(
                new DependItem("A", "B"),
                new DependItem("A", "C"),
                new DependItem("C", "B"),
                new DependItem("D", "A"),
                new DependItem("D", "C")
        ));

        Assertions.assertEquals(
                Arrays.asList(
                        new DependItem("C", "B"),
                        new DependItem("A", "B"),
                        new DependItem("A", "C"),
                        new DependItem("D", "A"),
                        new DependItem("D", "C")),
                sorted
        );
    }
}
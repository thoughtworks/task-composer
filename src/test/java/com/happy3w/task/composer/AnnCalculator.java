package com.happy3w.task.composer;

import java.util.Map;

public class AnnCalculator {
    public static final String S12 = "s12";
    public static final String M23 = "m23";

    public static @TaskData(AnnCalculator.S12) Integer sum(@TaskData(AnnTypeConvert.N1) Integer n1, @TaskData(AnnTypeConvert.N2) Integer n2) {
        return n1 + n2;
    }

    public static @TaskData(AnnCalculator.M23) Integer multi(
            @TaskData(AnnTypeConvert.N2)
            @TaskData(AnnTypeConvert.N3) Map<String, Object> params) {
        int v = 1;
        for (Object objV : params.values()) {
            v *= ((Integer) objV).intValue();
        }
        return v;
    }
}

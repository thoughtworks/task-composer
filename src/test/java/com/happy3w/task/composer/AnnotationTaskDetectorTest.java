package com.happy3w.task.composer;

import com.happy3w.toolkits.message.MessageRecorderException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

class AnnotationTaskDetectorTest {

    @Test
    void should_create_tasks_by_ann_and_run_success() {
        List<Task> tasks = AnnotationTaskDetector.detectTasks(new AnnTypeConvert(), AnnTypeConvert.class, AnnCalculator.class);
        TaskComposer taskComposer = TaskComposer.build(tasks);

        Integer s12Value = taskComposer
                .withValue(InputParams.N1Text, "111")
                .withValue(InputParams.N2Text, "222")
                .executorFor(AnnCalculator.S12)
                .withThreadCount(2)
                .withWaitTime(200)
                .getDataValue(AnnCalculator.S12);
        Assertions.assertEquals(333, s12Value.intValue());

        Integer m23Value = taskComposer
                .withValue(InputParams.N3Text, "333")
                .evaluate(AnnCalculator.M23);
        Assertions.assertEquals(222 * 333, m23Value.intValue());
    }

    @Test
    void should_show_error_when_lost_params() {
        List<Task> tasks = AnnotationTaskDetector.detectTasks(new AnnTypeConvert(), AnnTypeConvert.class, AnnCalculator.class);
        TaskComposer taskComposer = TaskComposer.build(tasks);

        Assertions.assertThrows(MessageRecorderException.class, () -> {
            taskComposer.evaluate(AnnCalculator.S12);
        });
    }

    @Test
    void should_collect_multi_return_success() {
        List<Task> tasks = AnnotationTaskDetector.detectTasks(ReturnMulti.class);
        Task task = tasks.get(0);
        Assertions.assertEquals(2, task.outputs.size());
    }

    public static class ReturnMulti {
        public static @TaskData("R1") @TaskData("R2") Map<String, Object> returnMulti() {
            return null;
        }
    }
}

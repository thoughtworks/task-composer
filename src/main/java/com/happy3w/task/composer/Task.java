package com.happy3w.task.composer;

import lombok.Getter;

import java.util.Collections;
import java.util.List;

@Getter
public abstract class Task {
    protected final List<TaskDataDef> inputs;
    protected final List<TaskDataDef> outputs;

    protected Task(List<TaskDataDef> inputs, List<TaskDataDef> outputs) {
        this.inputs = inputs;
        this.outputs = outputs;
    }

    public String outputNames() {
        StringBuilder buf = new StringBuilder();
        for (TaskDataDef output : getOutputs()) {
            if (buf.length() > 0) {
                buf.append('&');
            }
            buf.append(output.getName());
        }
        return buf.toString();
    }

    public List<TaskDataDef> getInputs() {
        return inputs == null ? Collections.EMPTY_LIST : inputs;
    }

    public List<TaskDataDef> getOutputs() {
        return outputs == null ? Collections.EMPTY_LIST : outputs;
    }

    public abstract void execute(TaskExecuteContext context);

    @Override
    public String toString() {
        return outputNames();
    }
}

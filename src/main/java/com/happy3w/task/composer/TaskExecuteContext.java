package com.happy3w.task.composer;

public interface TaskExecuteContext {
    Object getValue(String name);
    void setValue(String name, Object value);
}

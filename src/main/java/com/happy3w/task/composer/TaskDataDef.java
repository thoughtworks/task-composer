package com.happy3w.task.composer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.reflect.Type;

@Getter
@AllArgsConstructor
public class TaskDataDef<T> {
    private String name;
    private Type type;

    @Override
    public String toString() {
        return name + ':' + type;
    }
}

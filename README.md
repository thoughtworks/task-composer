# task-composer
This is a tool for run some tasks, These tasks has inputs and outputs, and some task's input is the other task's output.

## Quick start
```groovy
implementation 'com.happy3w:task-composer:1.0.3'

```

## Simple problem

A simple problem is like this:

```mermaid
flowchart TD
n1Text --> n1
n2Text --> n2
n3Text --> n3
n1 --> s12
n2 --> s12
n2 --> m23
n3 --> m23
```

n1Text,n2Text,n3Text are three text formatted number

n1,n2,n3 are number converted from n1Text,n2Text,n3Text

s12=n1+n2

m23=n2*n3


## Simple demo
A simple demo is like this:

```java
// Here is some variable names, These variable are all for input, not task output them.
public class InputParams {
    public static final String N1Text = "n1Text";
    public static final String N2Text = "n2Text";
    public static final String N3Text = "n3Text";
}

// Here I define 3 tasks for convert data,they are all demos, so I just let then convert string to int.
class AnnTypeConvert {
    
    // Here are some variable names, They will be output by tasks define here
    public static final String N1 = "n1";
    public static final String N2 = "n2";
    public static final String N3 = "n3";

    // This instance method is task. It's parameter and return is annotated with TaskData.This annotation is used to define where the parameter from and where to put the output.
    // This method will run with instance, so we can use class field in this method.
    public @TaskData(AnnTypeConvert.N1) Integer convert(@TaskData(InputParams.N1Text) String text) {
        return Integer.valueOf(text);
    }

    // This is a task with static method
    public static @TaskData(AnnTypeConvert.N2) Integer iconvert(@TaskData(InputParams.N2Text) String text) {
        return Integer.valueOf(text);
    }

    public static @TaskData(AnnTypeConvert.N3) Integer iconvert2(@TaskData(InputParams.N3Text) String text) {
        return Integer.valueOf(text);
    }
}

// Here is other task class. We can manage tasks in more class 
public class AnnCalculator {
    public static final String S12 = "s12";
    public static final String M23 = "m23";

    // This task is defined with multiple params in array parameter
    public static @TaskData(AnnCalculator.S12) Integer sum(@TaskData(AnnTypeConvert.N1) Integer n1, 
                                                           @TaskData(AnnTypeConvert.N2) Integer n2) {
        return n1 + n2;
    }

    // This task is defined with multiple params in map parameter
    // So we can create multiple output with map returned, with multiple annotation TaskData.
    public static @TaskData(AnnCalculator.M23) Integer multi(
            @TaskData(AnnTypeConvert.N2)
            @TaskData(AnnTypeConvert.N3) Map<String, Object> params) {
        int v = 1;
        for (Object objV : params.values()) {
            v *= ((Integer) objV).intValue();
        }
        return v;
    }
}
```
Now all tasks are defined, Then we will run them.
```java
// Use AnnotationTaskDetector collect all tasks in instances and types. 
List<Task> tasks = AnnotationTaskDetector.detectTasks(
  	// Used to collect task on instance
  	new AnnTypeConvert(), 
  	// Used to collect task on type
  	AnnTypeConvert.class, 
  	AnnCalculator.class);

// TaskComposer is used for run task in depend order, tasks without depend can be run parallel.
TaskComposer taskComposer = TaskComposer.build(tasks);

Integer s12Value = taskComposer
    // Input some params, We can input params just needed one
    .withValue(InputParams.N1Text, "111")
    .withValue(InputParams.N2Text, "222")
    // create the executor for calculate the special output
    .executorFor(AnnCalculator.S12)
    .withThreadCount(2) // config thead count for parallel run
    .withWaitTime(200)  // config the duration for check whether a task is can be run.
    .getDataValue(AnnCalculator.S12);   // run all necessary tasks and return the expect output.

// Start to calculate another output. It will reuse the task results calculated before.
Integer m23Value = taskComposer
    .withValue(InputParams.N3Text, "333")
    // just use the default config, and evaluate an output
    .evaluate(AnnCalculator.M23);

```
